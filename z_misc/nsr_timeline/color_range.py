#!/usr/bin/env python
#http://stackoverflow.com/questions/876853/generating-color-ranges-in-python

#import colorsys
#HSV_tuples = [(x*1.0/N, 0.5, 0.5) for x in range(N)]
#RGB_tuples = map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples)
#
#print RGB_tuples

# http://www.gossamer-threads.com/lists/python/python/819540

import colorsys

sat = 1
value = 1
length = 1000
for h in range(0, length + 1):
    hue = h / float(length)
    color = list(colorsys.hsv_to_rgb(hue, sat, value))
    for x in range(3):
        color[x] = int(color[x] * 255)
    hexval = ("#%02x%02x%02x" % tuple(color)).upper()
    print hexval 

