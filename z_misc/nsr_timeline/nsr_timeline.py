#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
usage:

    me [jobquery.txt]

example:

    echo -e ". type: savegroup job\nprint" | jobquery > jobquery.txt
    me jobquery.txt
"""
"""
TODO

- execCmd if no file-input is given
- out_idop  : json format suiting idop api (copy'n'pastebale)
- call_idop : png
- gen_report: rst2pdf
"""

import sys,os
import time,datetime
import string
import logging

FNAME="jobquery.txt"
GEN_IMAGE=True
#GEN_IMAGE=False

LOGFILE = "/tmp/bla"
logging.basicConfig(
#    filename=LOGFILE,
#    level=logging.DEBUG,
    level=logging.INFO,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("nsr_timeline")

__author__ = "s3h10r"
__version__= "0.0.2"


def execCmd(sCmd,env={}):
    """
    executes a given Command

    returns
        rc, output
    """
    import subprocess as sub

    p = sub.Popen([sCmd], shell=True, stdout=sub.PIPE, stderr=sub.STDOUT,env=env)
    #output = p.stdout.read()
    output = p.stdout.readlines()
    p.wait()
    rc = p.returncode

    if rc != 0:
        logger.critical("command failed with rc %i: %s" % (rc,sCmd))
        logger.critical("got %s" % (output))

    return rc, output


def proc_jobquery(f = None):
    """
    collect savegroup jobs
    with job-state "COMPLETED"
    """
    import re

    rx = r'(?P<k>[\w,\s]+):\s+(?P<v>[\w,\s]*)[\s+,\';\']'
    #compile_flags = 0
    compile_flags = re.UNICODE
    r = re.compile(rx,compile_flags) 

    lR = []

    nrJob = 0
    dJob = {}
    for l in f:
        l = l.strip()
        #logger.debug(l)
        #logger.debug(r.match(l))         
        #logger.debug(r.findall(l))

        found = r.findall(l)
        k = None; v = None
        if len(found) > 0:
            k = found[0][0].strip()
            v = found[0][1].strip()

        if k == 'type':
            if v == 'savegroup job':
                nrJob += 1
                if nrJob > 1:
                    lR.append(dJob)
                dJob = {}
            else:
                raise Exception, "type != 'savegroup job': %s" % v

        if k == 'name' or k == 'end time' or k == 'start time' or k == 'job state':
            if not dJob.has_key(k):
                dJob[k] = v
            else:
                raise Exception, "oops... :-("

    lR.append(dJob) # last job parsed


    # cleanup, plausi-checks ...

    lR_cln = []

    for d in lR:
        #print d
        if d['job state'] == 'COMPLETED':
            #print d['name'], d['start time'], d['end time']
            #d['_start_time_hr'] = datetime.datetime.fromtimestamp(float(d['start time'])).strftime('%Y-%m-%d %H:%M:%S')
            #d['_end_time_hr'] = datetime.datetime.fromtimestamp(float(d['end time'])).strftime('%Y-%m-%d %H:%M:%S')
            d['_start_time_hr'] = datetime.datetime.fromtimestamp(float(d['start time']))
            d['_end_time_hr'] = datetime.datetime.fromtimestamp(float(d['end time']))


            logger.debug("%s : %s - %s (dur: %s)" % (d['name'], d['_start_time_hr'], d['_end_time_hr'], d['_end_time_hr'] - d['_start_time_hr']))


            lR_cln.append(d)
        else:
            logger.info("ignoring job not completed: %s, start time: %s" % (d,datetime.datetime.fromtimestamp(float(d['start time'])).strftime('%Y-%m-%d %H:%M:%S')))
      
    return lR_cln 


def create_timeline(jobs = None, t_start = None, t_end = None, t_step_mins = 60):
    """
    build a timeline
    timline[<timeslot>]['name'] contains the groupnames
        a savegroup-job was running for in this slot (if a group is named more than once
        this means more than one savegroup-jobs for it were running inside the slot)
    """
    t_step = datetime.timedelta(minutes=t_step_mins)

    dTl = {} # Timeline
    names = []

    t_point = t_start 
    while t_point <= t_end:

        dTl[t_point] = []
        for d in jobs:
            if (d['_end_time_hr']  >= t_point and d['_start_time_hr'] <= t_point + t_step):
                if d['name'] in dTl[t_point]:
                    logger.warning("a savegroupjob for group %s is completed more than once in timeslot %s - %s" % (d['name'],t_point, t_point + t_step))

                dTl[t_point].append(d['name'])
                if d['name'] not in names:
                    names.append(d['name'])


        logger.info("%s : %i running savegroup jobs" % (t_point,len(dTl[t_point]))) # one group can run more than once in a timeslot
        logger.debug("    %s" % dTl[t_point])

        t_point += t_step

    return names,dTl    


def out_idop(timeline, names,title="nsr_timeline: savegrp jobs", colors=["#FF0000", "#00FF00", "#0000FF"]):
    """
    return json-format suiting idop api
    """

    import json

    """
{
  "type": "bar_stacked",
  "sizeX": "8", "sizeY" : "6", "dpi" : "100",

  "args" : {
      "title" : "parallel thingy / scheduler",

      "vals" : [ 
               [1, 1, 1, 1, 0, 0, 0, 0, 0 ],
               [0, 1, 1, 1, 1, 1, 0, 0, 0],
               [0, 0, 0, 1, 1, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 1, 1, 1, 1],
               [0, 0, 0, 1, 1, 1, 1, 1, 0]
 ],

      "val_legend" : ["Task 1", "Task 2", "Task 3","Task 4", "Task 5" ],

      "x_ticks" : ["0:00", "1:00", "2:00", "3:00", "4:00","5:00", "6:00","7:00","8:00","9:00" ],

      "y_label" : "nr. threads"
  }

}

    """

    x_ticks = sorted(timeline.keys())
    x_ticks = [ str(t) for t in x_ticks ]

    vals = []
    for name in names:
        val_line = []
        for t in sorted(timeline.keys()):
            # how often a savejob of group <name> runs in the timeline-slot (0,1 or n times)
            val_line.append(timeline[t].count(name))

        vals.append(val_line)

    names = [ name.decode(encoding='UTF-8',errors='ignore') for name in names ] # dirty workaround preventing json.dump failure on ä,ü bla
    val_legend = names
        
    data = { 'type': 'bar_stacked', \
             'sizeX' : 16, 'sizeY' : 12, 'dpi' : 100 }

    data_args = { 'title' : title, \
                  'vals'  : vals, \
                  'val_legend' : val_legend, \
                  'x_ticks' : x_ticks, \
                  'y_label' : 'nr. running savegrp-jobs', \
                  'bar_width' : 1.0, 'ind_x':0.0, \
                  'autofmt_xdate' : 0.18,
                  'legend_loc' : None,
                  'xtick_nbins' : 12,
                  'colors' : colors
                }

    data['args'] = data_args

    #print "JSON: ", json.dumps(data, sort_keys=True, indent=2)
    #print "JSON: ", json.dumps(data, sort_keys=True)
    jdata = json.dumps(data, sort_keys=True)

    return jdata        


def create_colors(amount=100):
    """
    http://www.gossamer-threads.com/lists/python/python/819540
    http://stackoverflow.com/questions/876853/generating-color-ranges-in-python
    """
    import colorsys

    colors = []
 
    sat = 1
    value = 1
    length = amount
    for h in range(0, length + 1):
        hue = h / float(length)
        color = list(colorsys.hsv_to_rgb(hue, sat, value))
        for x in range(3):
            color[x] = int(color[x] * 255)
        hexval = ("#%02x%02x%02x" % tuple(color)).upper()

        colors.append(hexval)

    colors = [ str(el) for el in colors ]

    return colors


def call_idop(jdata, url='http://localhost:8080/plot/bar_stacked'):
    """
    call idop-webapi to get chart as png-image
    (sends json-data via http-post) 

    returns png (as a memory file (StringIO))
    """

    import urllib2
    import cStringIO

    headers = {}
    headers['Content-Type'] = 'application/json'

    print "url           : ", url
    req = urllib2.Request(url, jdata, headers)
    rep = urllib2.urlopen(req)
    print rep.headers

    file_like = cStringIO.StringIO(rep.read())

    return file_like 


if __name__ == '__main__':

    if len(sys.argv) == 2: 
        FNAME = sys.argv[1]
    else:

        new_env = dict(os.environ)
        #print new_env

        print execCmd("date")
        new_env['LANG'] = 'de_DE.utf8'
        print execCmd("date",new_env)
        print execCmd("bash -c \"echo -e '. type: savegroup job\nprint'\" | jobquery > ./jobquery.txt",new_env)

    f = open(FNAME)
        

    lR = proc_jobquery(f)
    logger.info("nr. jobs completed total: %i " % len(lR))

    # filter jobs for all <= given timedelta (default: -1 day)
    jobs = []
    tdelta = datetime.timedelta(days=-3)
    now = datetime.datetime.now().replace(minute=0, second=0, microsecond=0)
    t_start = now + tdelta
    t_end = now

    for d in lR:
        if t_start <= d['_start_time_hr']:
            jobs.append(d)

    logger.info("nr. jobs suiting time-window %s - %s: %i" % (t_start,now,len(jobs)))

    #for name in names: print name.decode(encoding='UTF-8',errors='strict')
    names,timeline = create_timeline(jobs, t_start, t_end,t_step_mins=15) 
    logger.debug("nr. groups: %s (%s)" % (len(names),names))

    if not GEN_IMAGE: # plaintext output
        #names = [ name.decode(encoding='UTF-8',errors='ignore') for name in names ] # dirty workaround
        for t in sorted(timeline.keys()):
            print "\n\n", t 
            for g in timeline[t]:
                print g,

    else:
        colors = create_colors(len(names))
        title = "nsr_timeline: savegrp jobs (%i jobs, %i groups)" % (len(jobs),len(names))
        jdata = out_idop(timeline,names,title,colors)
        img = call_idop(jdata)
        f = open("./nsr_timeline.png","w")
        f.write(img.getvalue())
        f.close()

#        import PIL.Image
#        img = PIL.Image.open(img)
#        img.show()

        



