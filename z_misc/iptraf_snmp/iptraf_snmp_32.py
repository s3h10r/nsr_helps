#!/usr/bin/env python
"""
show ip traffic infos for a remote nic (snmp)

queries a 32-bit counter (should be available in most snmp-implementations)

usage:

    me [<host> <interface_nr> <check-interval_in_sec>]


!!! querying a 32bit-counter for octets (bytes) is a bad idea on high-traffic interfaces
and often results in false values (tricky ->  needs very short intervalls!) !!!

# welches intervall ist notwendig fuer ein voll ausgelastetes 1/10Gbit interface
# - nach wieviel zeit laeuft ein 32bit breiter byte-counter (ifEntryInOctets) ueber?

>>> (2**32 * 8) / (10**6 * 100.0) # 100Mbit/s
343.59738368
>>> (2**32 * 8) / (10**6 * 1000.0) # 1Gbit/s
34.359738368
>>> (2**32 * 8) / (10**6 * 2000.0) # 2Gbit/s
17.179869184
>>> (2**32 * 8) / (10**6 * 10000.0) # 10Gbit/s
3.4359738368

# Gegenprobe

>>> 2**32 - ( (3.4359738368 * (10**6 * 10000)) / 8.0 )
0.0
"""

"""
TODO:

- check_requirements() : snmpget, bla

- [-] ifEntrySpeed  = "1.3.6.1.2.1.2.2.1.5.$iface"; -> mindestintervall ausgeben / warnen

- logging info alle N intervalle a la read_csv.py:

	MiB_IN: 10380.862326 GiB_IN: 10.137561 TiB_IN: 0.009900
	MiB_OUT: 319.158313 GiB_OUT: 0.311678 TiB_OUT: 0.000304
	=> mean: 10.824674 MiB_IN 0.332803 MiB_OUT

- support 64bit counters (ifEntryIn64Octets) / switch between 32/64bit-counter

"""

import time,datetime
import string
import sys
import logging

HOST = "localhost"
IFACE = 2
INTERVAL=5 # secs

# ---

CNT_MAX_32 = 2**32 - 1
CNT_MAX_64 = 2**64 - 1

OID = {'ifEntryDesc'        : '1.3.6.1.2.1.2.2.1.2.', \
       'ifEntryStatus'      : '1.3.6.1.2.1.2.2.1.8.', \
       'ifEntrySpeed'       : '1.3.6.1.2.1.2.2.1.5.', \

       'ifEntryInOctets'    : '1.3.6.1.2.1.2.2.1.10.', \
       'ifEntryOutOctets'   : '1.3.6.1.2.1.2.2.1.16.', \

       'ifEntryIn64Octets'  : '1.3.6.1.2.1.31.1.1.1.6.', \
       'ifEntryOut64Octets' : '1.3.6.1.2.1.31.1.1.1.10.',
      }

LOGFILE = "/tmp/bla"

logging.basicConfig(
#    filename=LOGFILE,
    level=logging.DEBUG,
#    level=logging.INFO,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("iptraf_snmp")

__author__ = "s3h10r"
__version__= "0.0.1"


def execCmd(sCmd):
    """
    executes a given Command

    returns
        rc, output
    """
    import subprocess as sub

    p = sub.Popen([sCmd], shell=True, stdout=sub.PIPE, stderr=sub.STDOUT)
    #output = p.stdout.read()
    output = p.stdout.readlines()
    p.wait()
    rc = p.returncode

    if rc != 0:
        logger.critical("command failed with rc %i: %s" % (rc,sCmd))
        logger.critical("got %s" % (output))

    return rc, output


if __name__ == '__main__':

    if len(sys.argv) == 4: # host,iface,interval
        HOST = sys.argv[1]
        IFACE = sys.argv[2]
        INTERVAL = float(sys.argv[3])


    # --- show interface infos
    tplGET = string.Template('snmpget -c public -v 2c $HOST $OID$IFACE')
    for s in ['ifEntryDesc', 'ifEntryStatus', 'ifEntrySpeed']:
        cmd = tplGET.substitute({'HOST' : HOST, 'OID' : OID[s], 'IFACE': IFACE })
        logger.debug("cmd: %s" % cmd)
        rc, res = execCmd(cmd)
        logger.info("%s : %s" % (s,"".join(res[0]).strip()))


    # --- interface speed
    # CHECKME: may be unrealiable, got 1410065408 for an nxge0 via snmp and on os / in real it is a 10gbit-card:
    # 14:06:33 # dladm show-dev
    # nxge0           link: up        speed: 10000 Mbps       duplex: full

    
    tplGET = string.Template('snmpget -c public -v 2c $HOST $OID$IFACE | cut -d ":" -f 2')
    cmd = tplGET.substitute({'HOST' : HOST, 'OID' : OID['ifEntrySpeed'], 'IFACE': IFACE })
    rc, res = execCmd(cmd)
    speed_Mbps = float("".join(res[0])) / 10**6 # bits/s -> Mbits/s
    logger.info("interface speed: %f Mbit/s" % speed_Mbps)

    recom_interval = 2**32 * 8 / (10**6 * speed_Mbps) # TODO: check_func

    logger.info("recommended interval max. %f" % recom_interval)
    if INTERVAL > recom_interval:
        logger.critical("checkinterval %fs is too big for 32bit counter on ifspeed %f Mbps! recommended interval max.: %f" % (INTERVAL,speed_Mbps,recom_interval))
        logger.critical("!you may get unreliable and unsuitable results if the the interface is under heavy load!")


    # --- get datapoints
    tplGET = string.Template('snmpget -c public -v 2c $HOST $OID$IFACE | cut -d ":" -f 2')
    prev_cnt_IN, prev_cnt_OUT_last = None, None # previous values
    prev_ts = None
    while True:

        ts = time.time() # number of seconds since the epoch as seconds in UTC
        ts_hr = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S') # human readable

        cmd = tplGET.substitute({'HOST' : HOST, 'OID' : OID['ifEntryInOctets'], 'IFACE': IFACE })
        rc, res = execCmd(cmd)
        cnt_IN = int("".join(res))
        
        cmd = tplGET.substitute({'HOST' : HOST, 'OID' : OID['ifEntryOutOctets'], 'IFACE': IFACE })
        rc, res = execCmd(cmd)
        cnt_OUT = int("".join(res))

        if prev_cnt_IN == None:
            print "# ts;ts_hr;cnt_IN;cnt_OUT;bytesIN;bytesOUT; bpsIN; bpsOUT; MiBpsIN; MiBpsOUT"
            prev_cnt_IN = cnt_IN
            prev_cnt_OUT = cnt_OUT
            prev_ts = ts
        else:

            if cnt_IN >= prev_cnt_IN:
                bytesIN  = cnt_IN - prev_cnt_IN
            else: # counter overflow
                logger.debug("cnt_IN overflow: %i < %i" % (cnt_IN, prev_cnt_IN))
                bytesIN = CNT_MAX_32 - prev_cnt_IN + cnt_IN

            if cnt_OUT >= prev_cnt_OUT:
                bytesOUT  = cnt_OUT - prev_cnt_OUT
            else: # counter overflow
                logger.debug("cnt_OUT overflow: %i < %i" % (cnt_OUT, prev_cnt_OUT))
                bytesOUT = CNT_MAX_32 - prev_cnt_OUT + cnt_OUT

            delta_s = ts - prev_ts
            bpsIN = (bytesIN * 8) / delta_s
            bpsOUT = (bytesOUT * 8) / delta_s
            MBpsIN = (bytesIN / delta_s) / 1024.0**2 # MegaBytes per second IN
            MBpsOUT = (bytesOUT / delta_s) / 1024.0**2 # MegaBytes per second OUT

            print "%i;%s;%i;%i;%i;%i;%i;%i;%f;%f" % (ts,ts_hr,cnt_IN,cnt_OUT,bytesIN,bytesOUT,bpsIN,bpsOUT,MBpsIN,MBpsOUT)

            prev_cnt_IN = cnt_IN
            prev_cnt_OUT = cnt_OUT
            prev_ts = ts

        sys.stdout.flush() # nice if we're piping to tee and watch the screen in parallel

        time.sleep(INTERVAL)
         
