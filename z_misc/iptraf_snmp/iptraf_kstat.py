#!/usr/bin/env python
"""
shows ip traffic infos for a local nic via kstat (solaris10)

usage:

    me [<interface_grep> <check-interval_in_sec>]

example:

    me "nxge:0:nxge0" 60 
"""

"""
we're querying the 64bit counter 
"""

import time,datetime
import string
import sys
import logging

INTERVAL=30 # secs
IFACE_GREP="nxge:0:nxge0"

# ---

CNT_MAX_32 = 2**32 - 1
CNT_MAX_64 = 2**64 - 1

LOGFILE = "/tmp/bla"

logging.basicConfig(
#    filename=LOGFILE,
    level=logging.DEBUG,
#    level=logging.INFO,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("iptraf_kstat")

__author__ = "s3h10r"
__version__= "0.0.1"


def execCmd(sCmd):
    """
    executes a given Command

    returns
        rc, output
    """
    import subprocess as sub

    p = sub.Popen([sCmd], shell=True, stdout=sub.PIPE, stderr=sub.STDOUT)
    #output = p.stdout.read()
    output = p.stdout.readlines()
    p.wait()
    rc = p.returncode

    if rc != 0:
        logger.critical("command failed with rc %i: %s" % (rc,sCmd))
        logger.critical("got %s" % (output))

    return rc, output


if __name__ == '__main__':

    if len(sys.argv) == 3: # host,iface,interval
        IFACE_GREP = sys.argv[1]
        INTERVAL = float(sys.argv[2])


    # --- get datapoints
    tplGET = string.Template("kstat -p -m nxge | grep -i '$IFACE:$COUNTER' | cut -f 2")

    prev_cnt_IN, prev_cnt_OUT_last = None, None # previous values
    prev_ts = None
    while True:

        ts = time.time() # number of seconds since the epoch as seconds in UTC
        ts_hr = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S') # human readable

        cmd = tplGET.substitute({'IFACE' : IFACE_GREP, 'COUNTER': 'rbytes64' })
        rc, res = execCmd(cmd)
        cnt_IN = int("".join(res))
        
        cmd = tplGET.substitute({'IFACE' : IFACE_GREP, 'COUNTER': 'obytes64' })
        rc, res = execCmd(cmd)
        cnt_OUT = int("".join(res))

        if prev_cnt_IN == None:
            print "# ts;ts_hr;cnt_IN;cnt_OUT;bytesIN;bytesOUT; bpsIN; bpsOUT; MiBpsIN; MiBpsOUT"
            prev_cnt_IN = cnt_IN
            prev_cnt_OUT = cnt_OUT
            prev_ts = ts
        else:

            if cnt_IN >= prev_cnt_IN:
                bytesIN  = cnt_IN - prev_cnt_IN
            else: # counter overflow
                logger.debug("cnt_IN overflow: %i < %i" % (cnt_IN, prev_cnt_IN))
                bytesIN = CNT_MAX_64 - prev_cnt_IN + cnt_IN

            if cnt_OUT >= prev_cnt_OUT:
                bytesOUT  = cnt_OUT - prev_cnt_OUT
            else: # counter overflow
                logger.debug("cnt_OUT overflow: %i < %i" % (cnt_OUT, prev_cnt_OUT))
                bytesOUT = CNT_MAX_64 - prev_cnt_OUT + cnt_OUT

            delta_s = ts - prev_ts
            bpsIN = (bytesIN * 8) / delta_s
            bpsOUT = (bytesOUT * 8) / delta_s
            MBpsIN = (bytesIN / delta_s) / 1024.0**2 # MegaBytes per second IN
            MBpsOUT = (bytesOUT / delta_s) / 1024.0**2 # MegaBytes per second OUT

            print "%i;%s;%i;%i;%i;%i;%i;%i;%f;%f" % (ts,ts_hr,cnt_IN,cnt_OUT,bytesIN,bytesOUT,bpsIN,bpsOUT,MBpsIN,MBpsOUT)

            prev_cnt_IN = cnt_IN
            prev_cnt_OUT = cnt_OUT
            prev_ts = ts

        sys.stdout.flush() # nice if we're piping to tee and watch the screen in parallel

        time.sleep(INTERVAL)
         
