#!/usr/bin/python
"""
plots throughput of network interface card

reads output-file of iptraf-datacollector (iptraf_kstat, iptraf_snmp_*)

usage:
     me <csv-file> [<hostname> <iface_name>]
"""

import sys,re
import dateutil.parser

HOSTNAME="<hostname>"
IFACE="<iface_name>"

### matplotlib stuff

#import numpy as np
import matplotlib as mpl
##mpl.use("Agg") # agg-backend, so we can create figures without x-server (no PDF, just PNG etc.)
#mpl.use("Cairo") # cairo-backend, so we can create figures without x-server (PDF & PNG ...)
import matplotlib.pyplot as plt # we have to import the pyplot object AFTER setting backend

### logging
import logging,logging.config
### simple-logging config without conf-file
#LOGFILE="/tmp/bla.log"
#logging.basicConfig(filename=LOGFILE,level=logging.DEBUG,format= "%(asctime)s - %(name)s - [%(levelname)-8s] %(message)s")

logging.basicConfig(level=logging.DEBUG,format= "%(asctime)s - %(name)s - [%(levelname)-8s] %(message)s")
logger = logging.getLogger("idoplot")


__author__  = 'Sven Hessenmueller (sven.hessenmueller@gmail.com)'
__version__ = '0.1.1'
__date__ = "20110129"
__copyright__ = "Copyright (c) 2011 %s" % __author__
__license__ = "GPL"


def parseCSV(fname,delimiter=';'):
    """
    """
    import csv

    dct = {}

    f = open(fname,'rU')
    parser = csv.reader(f, delimiter=delimiter)
    firstRec = True
    lstFieldNames = None

    for fields in parser:
        if firstRec or not lstFieldNames:
            if fields[0].strip()[0] == "#":
                lstFieldNames = fields
            else:
                logger.warning("header not in first line? (no '#' hook?) - skipping: %s" % fields)
            firstRec = False
        else:
            if len(fields) != len(lstFieldNames):
                logger.warning("length of fields seems wrong - skipping: %s" % fields)
            else:
                for i,f in enumerate(fields):
                    if not len(dct.keys()) == len(lstFieldNames):
                        dct[lstFieldNames[i]] = []
                       
                    dct[lstFieldNames[i]].append(f)


    return dct


def trendgraph_date(lstNames,lstDates,lstValues,strTitle = "Title",strYLabel="GiB",lstColor=['#0000FF','#00FF00','#FF0000','#000000'],marker=""):
    """
    """
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    plt.title(strTitle)
    plt.ylabel(strYLabel)

    for i,name in enumerate(lstNames):
        #print i, name
        #print lstDates[i]
        #print lstValues[i]
    
        #ax1.plot_date(lstDates[i], lstValues[i],linestyle='-')
        #ax1.plot_date(lstDates[i], lstValues[i],linestyle='-'label=name,color=lstColor[i % len(lstColor)])
        ax1.plot_date(lstDates[i], lstValues[i],linestyle='-',linewidth=2.5,marker=marker,label=name,color=lstColor[i % len(lstColor)])

    fig.autofmt_xdate(bottom=0.18) # adjust for date labels display

    #plt.legend()
    plt.legend(loc="best") # locate the legend in the best way :-)
    plt.grid(True)
    plt.show()



def trendgraph_date_addsubplot(fig, subplotparm , lstNames,lstDates,lstValues,strTitle = "Title",strYLabel="GiB",lstColor=['#0000FF','#00FF00','#FF0000','#000000'],marker=""):
    ax1 = fig.add_subplot(subplotparm[0],subplotparm[1],subplotparm[2])
    plt.title(strTitle)
    plt.ylabel(strYLabel)

    for i,name in enumerate(lstNames):
        ax1.plot_date(lstDates[i], lstValues[i],linestyle='-',linewidth=2.5,marker=marker,label=name,color=lstColor[i % len(lstColor)])

    fig.autofmt_xdate(bottom=0.18) # adjust for date labels display

    plt.legend(loc="best") # locate the legend in the best way :-)
    plt.grid(True)



if __name__ == "__main__":

    if len(sys.argv) < 2:
        print __doc__
        sys.exit(0)

    fname = sys.argv[1]     
    data = parseCSV(fname)

    if len(sys.argv) == 4:
        HOSTNAME=sys.argv[2]
        IFACE=sys.argv[3]


    # [' MBpsIN', 'cnt_IN', ' bpsIN', 'bytesOUT', 'ts_hr', ' bpsOUT', ' MBpsOUT', '# ts', 'bytesIN', 'cnt_OUT']
    print data.keys() 

    lName = ['IN'] 
    lVal = [ data[' MBpsIN'] ]
    lTime = [ dateutil.parser.parse(el) for el in data['ts_hr'] ]
    lTime = [ lTime ]

    trendgraph_date(lName,lTime,lVal,"%s %s" % (HOSTNAME, IFACE),"MiB/s", lstColor=['#0000FF','#00FF00','#FF0000','#000000'])

    # --- draw with reference maximums 
    lName = ['IN']
    lVal = [ data[' MBpsIN'] ]
    lTime = [ dateutil.parser.parse(el) for el in data['ts_hr'] ]
    lTime = [ lTime ]

    max_MiBps_100Mbit = ((10.0**6 * 100) / 8) / 1024.0**2
    lName.append("100 Mbit (%.2f)" % max_MiBps_100Mbit)
    l1Mbit = [ max_MiBps_100Mbit ] * len(data[' MBpsIN'])
    lVal.append(l1Mbit)
    lTime.append(lTime[0])

    max_MiBps_1Gbit = ((10.0**6 * 1000) / 8) / 1024.0**2
    lName.append("1 Gbit (%.2f)" % max_MiBps_1Gbit)
    l1Gbit = [ max_MiBps_1Gbit ] * len(data[' MBpsIN'])
    lVal.append(l1Gbit)
    lTime.append(lTime[0])

    max_MiBps_2Gbit = ((10.0**6 * 2000) / 8) / 1024.0**2
    lName.append("2 Gbit (%.2f)" % max_MiBps_2Gbit)
    l2Gbit = [ max_MiBps_2Gbit ] * len(data[' MBpsIN'])
    lVal.append(l2Gbit)
    lTime.append(lTime[0])

    trendgraph_date(lName,lTime,lVal,"%s %s" % (HOSTNAME, IFACE),"MiB/s", lstColor=['#0000FF','#00FF00','#FF0000','#000000'])

    # ---

    # --- a plot with subplots
    #fig = plt.figure()
    #fig = plt.figure(figsize=(8,6),dpi=100) # 800x600
    fig = plt.figure(figsize=(32,8),dpi=100) # 3200x800
    nrSubplots = 2 

    lName = ['IN']
    lVal = [ data[' MBpsIN'] ]
    lTime = [ dateutil.parser.parse(el) for el in data['ts_hr'] ]
    lTime = [ lTime ]
    trendgraph_date_addsubplot(fig, [nrSubplots,1,1], lName,lTime,lVal, "%s: %s throughput" % (HOSTNAME,IFACE), "MiB/s", ['#0000FF'])

    lName = ['OUT']
    lVal = [ data[' MBpsOUT'] ]
    lTime = [ dateutil.parser.parse(el) for el in data['ts_hr'] ]
    lTime = [ lTime ]
    trendgraph_date_addsubplot(fig, [nrSubplots,1,2], lName,lTime,lVal, "%s: %s throughput" % (HOSTNAME,IFACE), "MiB/s", ['#00FF00'])



    #plt.legend(loc="best") # locate the legend in the best way :-)
    #plt.grid(True)
    plt.show()
    # ---


