#!/usr/bin/env python
"""
sumup MiBps
"""

import sys,csv

FN = "nsrserver_iptraf_kstat.csv"

if __name__ == '__main__':
    if len(sys.argv) > 1:
        FN = sys.argv[1]

    rdr = csv.reader(open(FN,"rb"),delimiter=";")

    MiB_IN = 0.0
    MiB_OUT = 0.0

    i = 0 # nr of datapoints
    for row in rdr:
        if row[0].find('#') >= 0:
            continue
        #print row

        MiB_IN += float(row[4]) / 1024.0**2
        MiB_OUT += float(row[5]) / 1024.0**2
        i += 1

    print "MiB_IN: %f GiB_IN: %f TiB_IN: %f" % (MiB_IN, MiB_IN / 1024, MiB_IN / 1024**2)
    print "MiB_OUT: %f GiB_OUT: %f TiB_OUT: %f" % (MiB_OUT, MiB_OUT / 1024, MiB_OUT / 1024**2)
    print "nr. datapoints: %i" % i
