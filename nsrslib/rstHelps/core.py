#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
some helpers for generating ReST-syntax

- tables
- (code-)snippets
- images
- headlines
"""

import StringIO,sys
import logging

LOGFILE = "/tmp/bla"

logging.basicConfig(
#    filename=LOGFILE,
    level=logging.DEBUG,
#    level=logging.INFO,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("rstHelps")


__author__ = 'Sven Hessenmüller (s3h10r@gmail.com)'
__url__ = 'http://querzone.de'
__version__ = '0.0.22'
__date__ = '20120404'
__copyright__ = 'Copyright (c) 2012 Sven Hessenmüller'
__license__ = "GPL"

def Table(dTab, console = False):
    """
    creates a table in ReST-syntax

    args
        console : print result to stdout

    returns
        string
    """

    DELIM = '|'

    # length of fields + delimiter + end_delimiter
    logger.debug("%i" % ( sum([el[-1] for el in dTab['header']]) + len(dTab['header']) + 1 ) )

    resultPlain = ""
    buf = StringIO.StringIO()
    orig_stdout = sys.stdout
    sys.stdout = buf # print >> buf 
    
    print "\n\n"

    ### print header

    lH = ''
    for h in dTab['header']:
        lH += '%s%s' % ('+','-'*h[-1])
    lH += "+"

    logger.debug("%i" % len(lH))

    print lH

    lH = DELIM
    lH += DELIM.join([el[0].ljust(el[1],' ') for el in dTab['header']])
    lH += DELIM
    print lH


    lH = ''
    for h in dTab['header']:
        lH += '%s%s' % ('+','='*h[-1])
    lH += "+"
    print lH


    ### print content
    for l in dTab['content']:
        lC = ''
        for i,cont in enumerate(l):
            lC += '%s%s' % (DELIM,unicode(cont).ljust(dTab['header'][i][-1], ' '))
        lC += DELIM
        lC = lC.encode('utf-8') # nargh, dirty-bugfix. see: print sys.getdefaultencoding()

        print lC

        # print row delimiter

        lF = ''
        for h in dTab['header']:
            lF += '%s%s' % ('+','-'*h[-1])
        lF += "+"
        print lF

    sys.stdout = orig_stdout
    #for l in buf.getvalue().split('\n'):
    #    resultPlain += l
    resultPlain = buf.getvalue()

    if console:
        print resultPlain

    return resultPlain


def _test_Table():
    print "\n\n**selftest rstTable()**"
    dTable={}
    dTable['header'] = [['head1',12],['head2',12],['head3',78]] # [name,length]
    dTable['content'] = [ ['cont1-1','cont1-2','cont1-3'], ['cont2-1','cont2-2','cont2-3'], ['cont3-1','cont3-2','cont3-3'] ]

    Table(dTable,console=True)

def _test2_Table():
    print "\n\n**selftest2 rstTable()**"
    dTable={}
    dTable['header'] = [['dez',12],['hex',12],['oct',12],['bin',12]] # [name,length]
    dTable['content'] = []

    for i in range(256):
        sDez = "%04d" % i; sHex = "%04X" % i; sOct = "%04o" % i;
        digits = 8;
        sBin = bin(i)[2:].rjust(digits,'0')
        
        lRow = [sDez,sHex,sOct,sBin]
        dTable['content'].append(lRow)

    Table(dTable, console=True)
        


def Snippet(content, token='::', indent=4, console=False):
    """
    """
    if (isinstance(content,file) or isinstance(content,StringIO.StringIO)): # file-like-object / handle
        f = content 
        f.seek(0)   # just to make sure we're on the beginning of content

    elif (isinstance(content,str)):
        try:
            f = open(content)
        except:
            try:
                f = content.split('\n')
            except:
                raise Exception, "hu? could not open content. sorry. :("

    resultPlain = ""
    buf = StringIO.StringIO()
    orig_stdout = sys.stdout
    sys.stdout = buf # print >> buf 

    print "\n",token, "\n"

    for l in f:
        print ' '*indent, l.strip()

    sys.stdout = orig_stdout
    resultPlain = buf.getvalue()
    if console:
        print resultPlain

    return resultPlain

    
def _test_Snippet():
    """
    """
    print "\n\n**selftest rstSnippet()**"
    Snippet('./core.py', token='.. code-block:: python',console=True)


def Img(fn = './example_figure.png', token='.. image::', console=False):

    resultPlain = ""
    buf = StringIO.StringIO()
    orig_stdout = sys.stdout
    sys.stdout = buf # print >> buf 

    print "\n\n"
    print token, fn

    sys.stdout = orig_stdout
    resultPlain = buf.getvalue()
    if console:
        print resultPlain
    
    return resultPlain

def _test_Img():
    print "\n\n**selftest rstImg()**"
    Img(console=True)

def Headline(hline = "this is a headline", level = 0, console = False):
    dLevel = {0 :'#', 1 : '=', 2 : '-', 3 : '_'}

    if not level in dLevel.keys():
        raise Exception, "sorry, level-value %i is unknown. valid are: %s" % (level,dLevel.keys())

    res = ""
    buf = StringIO.StringIO()
    orig_stdout = sys.stdout
    sys.stdout = buf # print >> buf 

    print hline
    print dLevel[level] * len(hline) 

    sys.stdout = orig_stdout
    res = buf.getvalue()
    if console:
        print res

    return res

def _test_Headline():
    for i in (1,2,3):

        Headline(hline = 'this is a headline of level %i' % i,level = i,console=True)


def selftest():
    print '''
================
rstHelps.core.py
================
:Author: %s
:Website: %s

:Description: simple helpers to twiddle with the plaintext markup ReST via Python

.. header::

   .. oddeven::

      .. class:: headertable

      +---+---------------------+------------------+
      |   |.. class:: centered  |.. class:: right  |
      |   |                     |                  |
      |   | ###Section###       |Page  ###Page###  |
      +---+---------------------+------------------+

      .. class:: headertable

      +----------------+---------------------+---+
      |                |.. class:: centered  |   |
      |                |                     |   |
      |Page ###Page### |    ###Section###    |   |
      +----------------+---------------------+---+


.. raw:: pdf

    PageBreak oneColumn

.. contents:: Table of Contents

.. section-numbering::

.. raw:: pdf

    PageBreak oneColumn
''' % (__author__, __url__)
    
    
    Headline("introspection", level=1, console=True)
    Headline("main docstring", level=2, console=True)
    print __doc__
#    Headline("funcs & it's docstrings", level=2, console=True)

    Headline("selftests", level=1, console=True)
    Headline("_test_Table()", level=2, console=True)
    _test_Table()
    Headline("_test_Table2()", level=2, console=True)
    _test2_Table()
    Headline("_test_Snippet()", level=2, console=True)
    _test_Snippet()
    Headline("_test_Img()", level=2, console=True)
    _test_Img()
    Headline("_test_Headline()", level=2, console=True)
    _test_Headline()

    Headline("PDF output", level=1, console=True)
    Snippet('''
$ apt-get install rst2pdf
$ cd ./rstHelps/ && ./core.py | rst2pdf -o /tmp/selftest.pdf
''',console=True)
    Headline("customizing style", level=2, console=True)
    Snippet('$ cd ./rstHelps/ && ./core.py | rst2pdf -s ./rst2pdf_style.json -o /tmp/selftest.pdf',console=True)


    
if __name__ == '__main__' :
    selftest()
