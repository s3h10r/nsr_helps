#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
shows defined savesets for clients

usage example:

    me
    me client1,client2,...,clientN
"""

"""
TODO
"""


import sys
import datetime
import logging

LOGFILE = "/tmp/bla"

logging.basicConfig(
#    filename=LOGFILE,
#    level=logging.DEBUG,
#    level=logging.INFO,
    level=logging.WARN,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("nsr_pusg")

import nsrslib.core as nsrsl
import rstHelps.core as rst

nsrsl.NSR_SERVER="localhost" # option to query a remote nsr-server

def getTimestamp(fmt="%Y-%m-%d %H:%M:%S"):
    import datetime

    return datetime.datetime.now().strftime(fmt)

if __name__ == '__main__':
    print __doc__

    RC = 0

    logger.info("using nsrslib version: %s" % nsrsl.__version__)

    clients = nsrsl.getClients()
  
    clients2query = None
    if len(sys.argv) > 1:
        clients2query = sys.argv[1].split(',') 
        clients2query = [ c.lower() for c in clients2query ]

    print "-"*68
    print "timestamp: ", getTimestamp(), "\n\n"
    for c in sorted(clients.keys()):
        if clients2query and not c in clients2query:
            continue

        print c
        for grp in clients[c].keys():
            print " "*2, "group: ", grp
            for sset in clients[c][grp]:
                print " "*4, sset
    print "-"*68

    sys.exit(RC)

