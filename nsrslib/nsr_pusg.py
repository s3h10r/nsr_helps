#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
infos about networker pool_usage & "prediction" of how many volumes
should be free for the next night / backup-window

compares amount of data_written in backup-window delta-days ago
against current available capacity.

(default delta: one week (-7 days))

usage:

    me [-n]
"""

"""
TODO
- usage: me [<sTS_BEGIN> <sTS_END>]
- output ReST #rheintxt
- optional commit of infos into a db # pool,writtenB,writtenGB,ts_start,ts_end
    - idop chart # nsrslib/plot_idop.py
"""


import sys
import datetime
import logging

LOGFILE = "/tmp/bla"

logging.basicConfig(
#    filename=LOGFILE,
#    level=logging.DEBUG,
#    level=logging.INFO,
    level=logging.WARN,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("nsr_pusg")

import nsrslib.core as nsrsl
import rstHelps.core as rst

DELTA_DAYS = -7 # default one week ago

DB="poolstat.sqlite" # TODO: optional commit of some infos into a db
USE_DB = False

if __name__ == '__main__':
    RC = 0

    logger.info("using nsrslib version: %s" % nsrsl.__version__)

    nsrsl.NSR_SERVER="localhost" # option to query a remote nsr-server

    if len(sys.argv) > 1:
        DELTA_DAYS=int(sys.argv[1])

    pools = nsrsl.getPools()
    pools_csv = nsrsl.getPoolsCSV(pools)

    print rst.Headline("actual poolusage",2)
    print rst.Snippet(pools_csv)

    # backup-window to query
    now = datetime.datetime.now()
    ts_begin = now + datetime.timedelta(days=DELTA_DAYS) # same day last week
    ts_begin = ts_begin.replace(hour=16,minute=0,second=0, microsecond=0) # backup-start window
    ts_end = ts_begin + datetime.timedelta(days=1) # 24h interval


    dTbl = {} # ReST table
    dTbl['header'] = [ ['pool',15], ['written_GiB',14], ['involved vols',14], ['assumed vols',14], ['available vols',14], ['available GiB',14 ] ]
    dTbl['content'] = []

    total_written_GB = 0.0

    for pool in pools:
        vols,sumsizeB = nsrsl.getPoolVolWritten(pool,ts_begin,ts_end) 

        if vols == None:
            logger.warning("pool: %s - assuming no matches found for the query" % pool)
            vols = []
            sumsizeB = 0

        nrvols_writ = len(vols) # nr vols written to inside "reference" backup-window
        sumsizeGB = sumsizeB/1024.0**3
        volsizeGB = 10.0 # assuming all volumes have same size
        nrvols_need = sumsizeGB / volsizeGB
        nrvols_have = pools[pool]['cntVolFree'] + pools[pool]['cntVolExpired']

        total_written_GB += sumsizeGB

        logger.info("pool: %s vols_written_to: %s gib_written: %s cur_free: %s cur_expired: %s" % (pool,nrvols_writ, sumsizeGB, pools[pool]['cntVolFree'], pools[pool]['cntVolExpired']))

#        print "%15s : prev. involved vols %7.2f needed vols %7.2f (%8.2f GiB) <= currently available %7.2f (%8.2f GiB)" % (pool,nrvols_writ,nrvols_need,sumsizeGB,nrvols_have, nrvols_have * volsizeGB),
#        if nrvols_need <= nrvols_have:
#            print "... ok."
#        else:
#            print "... NOPE!"
#            RC += 1

        row = [ pool, "%8.2f" % sumsizeGB, "%7.2f" % nrvols_writ, "%7.2f" % nrvols_need, "%7.2f" % nrvols_have, "%8.2f" % (nrvols_have * volsizeGB)]
        dTbl['content'].append(row)

    print "\n\n"
    print rst.Headline("are enough volumes available?",2) 
    print "prediction for %s based on amount of data written in backup window:" % now
    print "%s - %s" % (ts_begin, ts_end), "\n"
    print rst.Table(dTbl)
    print "\n"
    print "TOTAL previously written data : %10.2f GiB" % total_written_GB
    

    sys.exit(RC)

